import math
from numpy import polyfit
from scipy import interpolate, optimize
import argparse

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  CLASSES  - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class spectrum:
	#opening variables
	def __init__(self):
		self.name = ''
		self.energy = []
		self.counts = []
		self.interp = []
	
		self.size = 0
		self.average = 0

		self.angle_size = 0
	
	#size of the lists
	def get_size(self):
		self.size = len(self.energy)
		self.angle_size = len(self.counts[0])
	
	#average of counts
	def get_average(self):
		for n in self.counts:
			self.average += n
		self.average *= 1/self.size
	
	#create interpolation function
	def get_inter(self):
		for iAn in range(self.angle_size):
			counts_iAn = []
			for iEn in range(0, self.size):
				counts_iAn.append(self.counts[iEn][iAn])
			self.interp.append(interpolate.interp1d (self.energy, counts_iAn, kind='slinear'))

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  FUNCTIONS  - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

#read files and populate exp and sim objects
def read_file(exp_file_name, zipfile=None):
	#open variables
	exp = spectrum()

	#open files
	if zipfile == None:
		exp_file = open (exp_file_name,'r')
		exp_lines = exp_file.readlines()
	else:
		text = zipfile.read(exp_file_name).decode().strip()
		exp_lines = text.split('\n')

	#read lines
	for row in exp_lines:
		# split lines
		thisrow = row.split ()
		# get energy (first column)
		energy = float (thisrow [0])
		# store energy
		exp.energy.append(energy)

		# store counts
		counts = []
		for iAn in range(1, len(thisrow)):
			counts.append(float (thisrow [iAn]))
		exp.counts.append(counts)
		#print(energy, counts)

	#if energy is decreasing, invert the vectors
	if exp.energy[0] > exp.energy[-1]:
		exp_inv = spectrum()
		for i in range(1, len(exp.energy)+1):
			exp_inv.energy.append(exp.energy[exp.size-i])
			exp_inv.counts.append(exp.counts[exp.size-i])
		exp = exp_inv

	#get size of the lists
	exp.get_size()

	return exp

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def angle_sum(exp, exp_angle_min, exp_dangle, exp_new_angle_min, exp_new_angle_max):
	#open variables
	exp_new = spectrum()
	exp_new.energy = exp.energy
	
	for iEn in range(0, exp.size):
		counts_sum = 0
		for iAn in range(len(exp.counts[0])):
			angle = iAn*exp_dangle + exp_angle_min
			if angle >= exp_new_angle_min and angle < exp_new_angle_max:
				counts_sum += exp.counts[iEn][iAn]
				#print(exp.energy[iEn], angle, counts_sum)
		exp_new.counts.append([counts_sum])
		#print(exp.energy[iEn], counts_sum)
			
	#get size of the lists
	exp_new.get_size()

	return exp_new

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def interp(exp, exp_new_denergy):
	#create interpolation function
	print('Interpolating...')
	exp.get_inter()
	print('Done!')

	#open variables
	exp_new = spectrum()

	nEn = int((max(exp.energy) - min(exp.energy)) / exp_new_denergy) + 1

	for iEn in range(nEn):
		# store energy
		energy = iEn*exp_new_denergy + min(exp.energy)
		exp_new.energy.append(energy)
		# store counts
		
		counts = []
		for iAn in range(exp.angle_size):
			counts.append(exp.interp[iAn](energy))
		exp_new.counts.append(counts)
		#print(energy, counts)
	
	#get size of the lists
	exp_new.get_size()
	
	return exp_new

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def rebine(exp, exp_new_denergy):
	print('Rebining...')

	#open variables
	exp_new = spectrum()

	#total number of energy steps in the rebined spectrum
	nEn = int((max(exp.energy) - min(exp.energy)) / exp_new_denergy) + 1

	jEn = jEn0 = 0
	for iEn in range(nEn):
		energy = (iEn+0.5)*exp_new_denergy + min(exp.energy)
		counts = [0 for iAn in range(exp.angle_size)]

		while exp.energy[jEn] < energy + exp_new_denergy/2:
			if jEn == exp.size-1:
				denergy_j = exp.energy[jEn] - exp.energy[jEn-1]
			elif jEn > 0:
				denergy_j = min([exp.energy[jEn] - exp.energy[jEn-1], exp.energy[jEn+1] - exp.energy[jEn]])
			else:
				denergy_j = exp.energy[jEn+1] - exp.energy[jEn]
			for iAn in range(exp.angle_size):
				counts[iAn] += exp.counts[jEn][iAn] / denergy_j
			jEn += 1
			#test to see if the index is out of range
			if jEn >= exp.size:
				break

		# corrects for the number of bins summed inside dE_new
		for iAn in range(exp.angle_size):
			counts[iAn] = counts[iAn]/(jEn-jEn0) * exp_new_denergy

		# store energy
		exp_new.energy.append(energy)
		# store counts
		exp_new.counts.append(counts)

		jEn0 = jEn

	#get size of the lists
	exp_new.get_size()

	print('Done!')
	return exp_new

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def print_spectrum(filename, spec):
	with open(filename, 'w') as f:
		for iEn in range(spec.size):
			print(spec.energy[iEn], end='\t', file=f)
			for iAn in range(spec.angle_size):
				print(spec.counts[iEn][iAn], end='\t', file=f)
			print('', file=f)


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - MAIN  - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Sum an angle window and rebin a 2D MEIS data file.')
	parser.add_argument('file', type=str, help='2D MEIS spectrum\'s file name')
	parser.add_argument('amin', type=float, help='first angle in the angle window to sum')
	parser.add_argument('amax', type=float, help='last angle in the angle window to sum')
	parser.add_argument('-o', type=str, help='output spectrum\'s file name (default=dmeis.out)', default='dmeis.out')
	parser.add_argument('-d', type=float, help='new energy bin size (default=0.1)', default=0.1)
	parser.add_argument('-m', type=float, help='2D MEIS spectrum\'s minimum angle (default=108)', default=108)
	parser.add_argument('-a', type=float, help='2D MEIS spectrum\'s angle bin size (default=0.08)', default=0.08)

	args = parser.parse_args()

	#read the files
	exp = read_file(args.file)

	exp_sum = angle_sum(exp, args.m, args.a, args.amin, args.amax)
	#exp_final = exp_sum
	exp_final = rebine(exp_sum, args.d)

	print_spectrum(args.o, exp_final)
